package br.com.ioasys.api.service;

import br.com.ioasys.api.config.CacheCustom;
import br.com.ioasys.api.dto.CredentialsDto;
import br.com.ioasys.api.entity.AuthRequest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class IoasysAuthService {

    public CredentialsDto getAcessCredential(AuthRequest authRequest) {
        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

        Map<String, Object> map = new HashMap<>();
        map.put("email", authRequest.getEmail());
        map.put("password", authRequest.getPassword());

        HttpEntity<Map<String, Object>> entity = new HttpEntity<>(map, headers);
        headers = restTemplate.postForEntity("https://empresas.ioasys.com.br/api/v1/users/auth/sign_in", entity, String.class).getHeaders();
        return new CredentialsDto(headers.getValuesAsList("uid").get(0), headers.getValuesAsList("access-token").get(0), headers.getValuesAsList("client").get(0));
    }

    public static HttpHeaders buildHeaderIoasys(String emailRequest) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.set("access-token", CacheCustom.cache.get(emailRequest).acessToken);
        headers.set("client", CacheCustom.cache.get(emailRequest).client);
        headers.set("uid", CacheCustom.cache.get(emailRequest).uid);
        return headers;
    }
}
