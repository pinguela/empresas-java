package br.com.ioasys.api.dto;

public class EmpresaDto {
    public Long id;
    public String enterprise_name;
    public String description;
    public String email_enterprise;
    public String facebook;
    public String twitter;
    public String linkedin;
    public String phone;
    public Boolean own_enterprise;
    public String photo;
    public Long value;
    public Long shares;
    public Double share_price;
    public Long own_shares;
    public String city;
    public String country;
    public EnterpriseDtoType enterprise_type;
}
