package br.com.ioasys.api.controller;

import br.com.ioasys.api.GlobalConfig;
import br.com.ioasys.api.config.CacheCustom;
import br.com.ioasys.api.dto.CredentialsDto;
import br.com.ioasys.api.dto.ResponseDto;
import br.com.ioasys.api.entity.AuthRequest;
import br.com.ioasys.api.entity.Usuario;
import br.com.ioasys.api.service.IoasysAuthService;
import br.com.ioasys.api.util.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/v1/")
public class AutenticateController {

    @Autowired
    private JwtUtil jwtUtil;

    @Autowired
    private AuthenticationManager authenticationManager;

    @GetMapping("enterprises")
    public ResponseEntity<ResponseDto> boasVindas(HttpServletRequest request) throws IOException, URISyntaxException {

        HttpEntity httpEntity = new HttpEntity(IoasysAuthService.buildHeaderIoasys(request.getAttribute("uid_token").toString()));
        UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(GlobalConfig.URL_IOASYS+ request.getRequestURI());

        request.getParameterMap().forEach((s, strings) ->
                uriBuilder.queryParam(s, Arrays.stream(strings).findFirst().get()));

        return new RestTemplate().exchange(uriBuilder.toUriString(), HttpMethod.GET, httpEntity, ResponseDto.class);
    }

    @GetMapping("enterprises/{id}")
    public ResponseEntity<ResponseDto> boasVindas(HttpServletRequest request, @PathVariable Long id) throws IOException, URISyntaxException {

        HttpEntity httpEntity = new HttpEntity(IoasysAuthService.buildHeaderIoasys(request.getAttribute("uid_token").toString()));
        UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(GlobalConfig.URL_IOASYS + request.getRequestURI() + id);

        return new RestTemplate().exchange(uriBuilder.toUriString(), HttpMethod.GET, httpEntity, ResponseDto.class);
    }

    @PostMapping("users/auth/sign_in")
    public String generateToken(@RequestBody AuthRequest authRequest, HttpServletRequest request) throws Exception {
        try {

            CredentialsDto credentialIoasys = new IoasysAuthService().getAcessCredential(authRequest);
            CacheCustom.setCache(authRequest.getEmail(), credentialIoasys);

            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(authRequest.getEmail(), authRequest.getPassword())
            );
        } catch (Exception ex) {
            throw new Exception("Email ou password inválido!");
        }
        return jwtUtil.generateToken(authRequest.getEmail());
    }
}
