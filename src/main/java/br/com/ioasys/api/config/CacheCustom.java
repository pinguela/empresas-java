package br.com.ioasys.api.config;

import br.com.ioasys.api.dto.CredentialsDto;

import java.util.HashMap;
import java.util.Map;

public class CacheCustom {
    public static Map<String, CredentialsDto> cache = new HashMap<>();

    public static void setCache(String sessionId, CredentialsDto credentialIoasys) {
        cache.put(sessionId, credentialIoasys);
    }

}
