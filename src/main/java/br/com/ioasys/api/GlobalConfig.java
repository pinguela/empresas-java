package br.com.ioasys.api;

public class GlobalConfig {
    public static final String SCHEME = "api_empresa";
    public static final String SECRET = "ioasys_TOKEN_secret";
    public static final String URL_IOASYS = "https://empresas.ioasys.com.br/";
}
